# Jenkins LTS JDK11

This is a fully functional Jenkins server, based on LTS releases.

## Getting Started

These instructions will cover usage information and for the docker container 

### Prerequisities

In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

`docker run -p 8080:8080 -p 50000:50000 registry.gitlab.com/docker-images-bk/jenkins-lts-jdk11:latest`

NOTE: read the section [Connecting agents](https://github.com/jenkinsci/docker/blob/master/README.md#connecting-agents) below for the role of the 50000 port mapping.

`docker run -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home registry.gitlab.com/docker-images-bk/jenkins-lts-jdk11:latest`

NOTE: This will automatically create a 'jenkins_home' docker volume on the host machine. Docker volumes retain their content even when the container is stopped, started, or deleted.

NOTE: Avoid using a bind mount from a folder on the host machine into /var/jenkins_home, as this might result in file permission issues (the user used inside the container might not have rights to the folder on the host machine). If you really need to bind mount jenkins_home, ensure that the directory on the host is accessible by the jenkins user inside the container (jenkins user - uid 1000) or use -u some_other_user parameter with docker run.

You can access logs with command 'docker logs CONTAINER_ID' in order to check first login token. ID of container will be returned from output of command above.

#### Ports

* `8080` - Web Access
* `50000` - [Connecting agents](https://github.com/jenkinsci/docker/blob/master/README.md#connecting-agents)

#### Volumes

* `/var/jenkins_home` - All Jenkins data lives in there - including plugins and configuration.
